# API Gateway emulator

This is an _experimental_ emulator for the API Gateway

The docker packaging in this repo will spin up a proxy listening on port 9000
which forwards requests to the URL pointed to by the
`GATEWAY_PROXY_DESTINATION_URL` environment variable.

A proxy configuration UI is at port 9001.

Requests will be authenticated with a JWT. The issuer (`iss`), audience (`aud`) and authorised party
(`azp`) claims of the JWT can be set via the `GATEWAY_JWT_ISS`, `GATEWAY_JWT_AUD` and
`GATEWAY_JWT_AZP` environment variables. If not provided, they will default to
`http://localhost:9001/`, `https://audience.invalid/` and
`api-gateway@api-meta-2555105a.iam.gserviceaccount.com` respectively.

The key id used to sign the JWTs can be set via `GATEWAY_JWT_KEY_ID` if necessary. If not
provided, a random key id will be used.

A JSON document of the form `{"[key id]": "[certificate]"}` can be fetched from
`http://localhost:9001/certs`. A standard JSON Web Key Set (JWKS) document can be fetched from
`http://localhost:9001/.well-known/jwks.json`. Just enough of an OpenID discovery document to make
the https://jwt.io/ debugger happy is available at
`http://localhost:9001/.well-known/openid-configuration`. **NOTE THE PORT NUMBER. THE CERTIFICATE
AND JWKS DOCUMENTS ARE HOSTED FROM THE CONSOLE UI, NOT THE PROXY.**

Initial proxy configuration can be influenced by other `GATEWAY_...` environment variables. See the
[definition of the Settings class](./api_gateway_emulator/__init__.py).

## Backend development

You can start a simple development instance via `docker compose up --build`. **CHANGES ARE NOT HOT
RELOADED.** If you make a change, you will need to restart the server via `docker compose down` and
`docker compose up --build`. The proxied application is a simple echo server which returns details
of the incoming request as a JSON document.

The issuer for the JWT is set such that you should be able to copy the JWT passed to the backend and
paste it into the https://jwt.io/ debugger and be able to validate the signature.

## Backend development

Install pipx following [the instructions on their site](https://pypa.github.io/pipx/), then
install the Python dependency manager and task runner:

```bash
$ pipx install poetry
$ pipx install poethepoet
```

The development environment can then be setup:

```bash
$ poetry install
$ poe setup
```

The pre-commit code checks (linting) should be performed automatically when committing to git
(assuming `poe setup` has previously been run). To run the checks manually:

```bash
$ poe fix
```

## Frontend development

You can install and start a development server as usual by running `yarn install` and `yarn start`
in the `frontend/` directory.

The application is available at [http://localhost:3000/](http://localhost:3000/).

The development server automatically proxies API calls to
[http://localhost:9001/](http://localhost:9001/) so you can start an instance of the gateway
emulator with default settings and it should Just Work (TM).
