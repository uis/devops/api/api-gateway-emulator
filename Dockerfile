###################################################################################################
# Image used to build frontend.
FROM node:lts AS frontend-builder

WORKDIR /usr/src/app
COPY ./frontend/package.json ./frontend/yarn.lock .

# HACK: Yarn will sometimes timeout downloading packages when initially
# building images. We set the timeout to avoid this.
RUN yarn config set network-timeout 600000 -g && yarn install

COPY ./frontend/ .
RUN yarn build

###################################################################################################
# Production image
FROM registry.gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/django:4.2-py3.11-alpine AS production
ENV PYTHONUNBUFFERED=1 PYTHONDONTWRITEBYTECODE=1

WORKDIR /usr/src/app

RUN --mount=type=bind,target=/context pip install /context
COPY --from=frontend-builder /usr/src/app/build/ ./frontend/build/

ENV GATEWAY_CONSOLE_UI_DIR=/usr/src/app/frontend/build

HEALTHCHECK --interval=10s CMD wget --spider http://127.0.0.1:9001/proxy/status
ENTRYPOINT ["api-gateway-emulator"]
