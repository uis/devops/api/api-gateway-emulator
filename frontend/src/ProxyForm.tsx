import React from "react";

import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Card from '@mui/material/Card';
import Grid from "@mui/material/Grid";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import MenuItem from "@mui/material/MenuItem";
import InputAdornment from "@mui/material/InputAdornment";
import TextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";
import { MuiChipsInput } from "mui-chips-input";
import { useSnackbar } from "notistack";

import { useProxyState, ProxyState, useProxyConfigurationMutation } from "./hooks";

interface FormState {
  isStateModified: boolean;
  hasFetchedOnce: boolean;
  pathPrefix: string;
  userType: "person" | "application" | "custom";
  userIdentifier: string;
  userScopes: string[];
  clientId: string;
  clientGrantType: "client_credentials" | "code";
  applicationId: string;
  applicationClass: "public" | "confidential";
  organisationId: string;
  maxRequestSize: number;
}

const INITIAL_FORM_STATE: FormState = {
  isStateModified: false,
  hasFetchedOnce: false,
  pathPrefix: "/",
  userType: "application",
  userIdentifier: "",
  userScopes: [],
  clientId: "",
  clientGrantType: "code",
  applicationId: "",
  applicationClass: "public",
  organisationId: "",
  maxRequestSize: 0,
};

const USER_SUFFICES = {
  person: "@v1.person.identifiers.cam.ac.uk",
  application: "@application.api.apps.cam.ac.uk",
  custom: "",
};

type FormErrors = { [key in keyof FormState]?: string };

const validateState = (state: FormState): FormErrors => {
  const errors: FormErrors = {};

  if (!state.pathPrefix.startsWith("/")) {
    errors.pathPrefix = "Path prefix must start with a slash.";
  }

  if (state.userIdentifier === "") {
    errors.userIdentifier = "Identifier must not be blank.";
  } else if (state.userType !== "custom" && state.userIdentifier.includes("@")) {
    errors.userIdentifier = 'Identifier must not include "@".';
  } else if (state.userType === "custom" && !state.userIdentifier.includes("@")) {
    errors.userIdentifier = 'Custom identifiers must include "@".';
  }

  if (state.clientId === "") {
    errors.clientId = "Client id must not be blank.";
  }

  if (state.applicationId === "") {
    errors.applicationId = "Application id must not be blank.";
  }

  if (state.organisationId === "") {
    errors.organisationId = "Organisation id must not be blank.";
  }

  return errors;
};

const applyProxyState = (proxyState: ProxyState, formState: FormState): FormState => {
  const userFields: Partial<FormState> = {
    userType: "custom",
    userIdentifier: proxyState.oauth2User,
  };
  for (const [userType, suffix] of Object.entries(USER_SUFFICES)) {
    if (userType !== "custom" && proxyState.oauth2User.endsWith(suffix)) {
      userFields.userType = userType as FormState["userType"];
      userFields.userIdentifier = proxyState.oauth2User.slice(0, -suffix.length);
    }
  }

  return {
    ...formState,
    isStateModified: false,
    pathPrefix: proxyState.pathPrefix,
    userScopes: proxyState.scopes === "" ? [] : proxyState.scopes.split(" "),
    clientId: proxyState.clientId,
    clientGrantType: proxyState.clientGrantType as FormState["clientGrantType"],
    applicationId: proxyState.applicationId,
    applicationClass: proxyState.applicationClass as FormState["applicationClass"],
    organisationId: proxyState.organisationId,
    ...userFields,
  };
};

const ProxyForm = () => {
  const { enqueueSnackbar } = useSnackbar();

  const [state, setState] = React.useState<FormState>(INITIAL_FORM_STATE);
  const [errors, setErrors] = React.useState<{ [key in keyof FormState]?: string }>({});

  const { data: fetchedState } = useProxyState();
  const { mutate: mutateConfiguration } = useProxyConfigurationMutation();

  const canApply = Object.keys(errors).length === 0 && state.isStateModified;
  const canReset = !!fetchedState && state.isStateModified;

  const patchState = (patch: Partial<FormState>) =>
    setState((prevState) => {
      const newState = {
        ...prevState,
        ...patch,
        isStateModified: true,
      };
      return newState;
    });

  // If we have a state from the API, update our current state if we're not currently modified.
  React.useEffect(() => {
    if (!fetchedState || state.isStateModified) {
      return;
    }
    setState((prev) => {
      const newState = { ...applyProxyState(fetchedState, prev), hasFetchedOnce: true };
      setErrors(validateState(newState));
      return newState;
    });
  }, [state.isStateModified, fetchedState, setErrors]);

  React.useEffect(() => {
    setErrors(validateState(state));
  }, [setErrors, state]);

  const handleApply = () => {
    mutateConfiguration(
      {
        pathPrefix: state.pathPrefix,
        scopes: state.userScopes.join(" "),
        oauth2User: `${state.userIdentifier}${USER_SUFFICES[state.userType]}`,
        clientId: state.clientId,
        clientGrantType: state.clientGrantType,
        applicationId: state.applicationId,
        applicationClass: state.applicationClass,
        organisationId: state.organisationId,
      },
      {
        onSuccess: () => {
          setState((prev) => ({ ...prev, isStateModified: false }));
          enqueueSnackbar("Configuration updated.", { variant: "success" });
        },
        onError: () => {
          enqueueSnackbar("There was en error updating the configuration.", { variant: "error" });
        },
      }
    );
  };

  if (!state.hasFetchedOnce) {
    return <></>;
  }

  return (
    <Box
      component="form"
      display="flex"
      flexDirection="column"
      onSubmit={(e) => {
        e.preventDefault();
      }}
    >
      <Typography variant="subtitle1">Proxy</Typography>
      <TextField
        fullWidth
        margin="normal"
        label="Path prefix"
        value={state.pathPrefix}
        error={!!errors.pathPrefix}
        helperText={errors.pathPrefix}
        onChange={({ target: { value: pathPrefix } }) => patchState({ pathPrefix })}
      />

      <Typography variant="subtitle1">Auth token properties</Typography>
      <Grid container spacing={2}>
        <Grid item xs={3}>
          <TextField
            fullWidth
            label="User type"
            select
            value={state.userType}
            margin="normal"
            onChange={({ target: { value } }) =>
              patchState({
                userType: value as FormState["userType"],
              })
            }
          >
            <MenuItem value="person">Person</MenuItem>
            <MenuItem value="application">Application</MenuItem>
            <MenuItem value="custom">Custom</MenuItem>
          </TextField>
        </Grid>
        <Grid item xs={9}>
          <TextField
            fullWidth
            label="User identifier"
            value={state.userIdentifier}
            onChange={({ target: { value: userIdentifier } }) => patchState({ userIdentifier })}
            error={!!errors.userIdentifier}
            helperText={errors.userIdentifier}
            margin="normal"
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">{USER_SUFFICES[state.userType]}</InputAdornment>
              ),
            }}
          />
        </Grid>
      </Grid>
      <MuiChipsInput
        fullWidth
        margin="normal"
        label="Scopes"
        placeholder="Press enter to add scopes"
        InputLabelProps={{ shrink: state.userScopes.length > 0 ? true : undefined }}
        value={state.userScopes}
        onChange={(userScopes) => patchState({ userScopes })}
        validate={(value) => ({
          isError: value.includes(" "),
          textError: "Scopes may not contain spaces.",
        })}
        disableEdition
      />

      <Typography variant="subtitle1">Client</Typography>
      <Grid container spacing={2}>
        <Grid item xs={8}>
          <TextField
            fullWidth
            label="Client id"
            margin="normal"
            value={state.clientId}
            error={!!errors.clientId}
            helperText={errors.clientId}
            onChange={({ target: { value: clientId } }) => patchState({ clientId })}
          />
        </Grid>
        <Grid item xs={4}>
          <TextField
            fullWidth
            label="Grant type"
            select
            margin="normal"
            value={state.clientGrantType}
            onChange={({ target: { value } }) =>
              patchState({ clientGrantType: value as FormState["clientGrantType"] })
            }
          >
            <MenuItem value="code">Auth code</MenuItem>
            <MenuItem value="client_credentials">Client credentials</MenuItem>
          </TextField>
        </Grid>
      </Grid>
      <Grid container spacing={2}>
        <Grid item xs={8}>
          <TextField
            fullWidth
            label="Application id"
            margin="normal"
            value={state.applicationId}
            onChange={({ target: { value: applicationId } }) => patchState({ applicationId })}
            error={!!errors.applicationId}
            helperText={errors.applicationId}
          />
        </Grid>
        <Grid item xs={4}>
          <TextField
            fullWidth
            label="Application Class"
            select
            margin="normal"
            value={state.applicationClass}
            onChange={({ target: { value } }) =>
              patchState({ applicationClass: value as FormState["applicationClass"] })
            }
          >
            <MenuItem value="public">Public</MenuItem>
            <MenuItem value="confidential">Confidential</MenuItem>
          </TextField>
        </Grid>
      </Grid>
      <Typography variant="subtitle1">Organisation</Typography>
      <TextField
        fullWidth
        label="Organisation id"
        margin="normal"
        value={state.organisationId}
        onChange={({ target: { value: organisationId } }) => patchState({ organisationId })}
        error={!!errors.organisationId}
        helperText={errors.organisationId}
      />
      <Typography variant="subtitle1">Read-only settings</Typography>
      <Card variant="outlined">
        <List dense>
          <ListItem
            secondaryAction={
              <Typography variant="body2">
                {fetchedState ? fetchedState.maxRequestSize : ""}
              </Typography>
            }
          >
            <ListItemText
              primary="Maximum HTTP request size (bytes)"
            />
          </ListItem>
        </List>
      </Card>
      <Grid container spacing={2} marginTop={1}>
        <Grid item xs={6}>
          <Button
            fullWidth
            size="large"
            variant="outlined"
            disabled={!canReset}
            onClick={() => fetchedState && setState((prev) => applyProxyState(fetchedState, prev))}
          >
            Reset
          </Button>
        </Grid>
        <Grid item xs={6}>
          <Button
            fullWidth
            disabled={!canApply}
            size="large"
            variant="contained"
            onClick={handleApply}
            type="submit"
          >
            Apply
          </Button>
        </Grid>
      </Grid>
    </Box>
  );
};

export default ProxyForm;
