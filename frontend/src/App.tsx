import React from "react";

import useMediaQuery from "@mui/material/useMediaQuery";
import CssBaseline from "@mui/material/CssBaseline";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";

import ProxyForm from "./ProxyForm";

import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/700.css";

const App = () => {
  const prefersDarkMode = useMediaQuery("(prefers-color-scheme: dark)");
  const theme = React.useMemo(
    () =>
      createTheme({
        palette: {
          mode: prefersDarkMode ? "dark" : "light",
        },
      }),
    [prefersDarkMode]
  );

  return (
    <ThemeProvider theme={theme}>
      <Box
        margin={3}
        sx={{
          display: "flex",
          flexDirection: "column",
          maxWidth: "600px",
          marginLeft: "auto",
          marginRight: "auto",
        }}
      >
        <CssBaseline />
        <Box
          sx={{
            width: "600px",
          }}
        >
          <Typography variant="h4" gutterBottom>API Gateway Emulator Configuration</Typography>
          <ProxyForm />
        </Box>
      </Box>
    </ThemeProvider>
  );
};

export default App;
