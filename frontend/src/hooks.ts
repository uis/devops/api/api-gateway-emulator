import {
  useQuery,
  useMutation,
  useQueryClient,
  UseQueryOptions,
  UseMutationOptions,
} from "@tanstack/react-query";

export interface ProxyState {
  destinationUrl: string;
  pathPrefix: string;
  extraHeaders: string;
  host: string;
  port: number;
  oauth2User: string;
  scopes: string;
  clientId: string;
  clientGrantType: string;
  applicationId: string;
  applicationClass: string;
  organisationId: string;
  maxRequestSize: number;
}

export interface ProxyConfigurationPatch {
  pathPrefix?: string;
  oauth2User?: string;
  scopes?: string;
  clientId?: string;
  clientGrantType?: string;
  applicationId?: string;
  applicationClass?: string;
  organisationId?: string;
}

/**
 * Wrap fetch so that it throws an Error if the request does not succeed.
 */
const wrappedFetch = async <TData>(...args: Parameters<typeof fetch>): Promise<TData> => {
  const response = await fetch(...args);
  if (!response.ok) {
    throw new Error("Error response.");
  }
  return await response.json();
};

/**
 * Get the current state of the proxy.
 */
export const useProxyState = (
  options?: Omit<UseQueryOptions<ProxyState, unknown, ProxyState, string[]>, "queryFn" | "queryKey">
) =>
  useQuery({
    queryKey: ["proxy", "state"],
    queryFn: () => wrappedFetch<ProxyState>("/proxy/status"),
    ...options,
  });

/**
 * Set the current state of the proxy.
 */
export const useProxyConfigurationMutation = (
  options?: Omit<UseMutationOptions<ProxyState, unknown, ProxyConfigurationPatch>, "mutationFn" | "mutationKey">
) => {
  const queryClient = useQueryClient();
  return useMutation({
    mutationKey: ["proxy", "configuration"],
    mutationFn: (configurationPatch: ProxyConfigurationPatch) =>
      wrappedFetch<ProxyState>("/proxy/configuration", {
        method: "PATCH",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(configurationPatch),
      }),
    ...options,
    onSuccess: (newState, ...args) => {
      queryClient.setQueryData(["proxy", "state"], newState);
      return options?.onSuccess ? options.onSuccess(newState, ...args) : undefined;
    },
  });
};
