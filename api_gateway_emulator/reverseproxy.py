import urllib.parse

import aiohttp.client
import aiohttp.web
import structlog

LOG = structlog.get_logger()


class ReverseProxy:
    """
    Configurable reverse HTTP proxy.
    """

    def __init__(
        self,
        destination_url,
        jwt_factory,
        *,
        host="0.0.0.0",
        port=8899,
        path_prefix="/",
        extra_headers=None,
        oauth2_user="test@example.invalid",
        scopes="",
        client_id="",
        client_grant_type="",
        application_id="",
        application_class="",
        organisation_id="",
        max_request_size=0,
    ):
        self.destination_url = destination_url
        self.jwt_factory = jwt_factory
        self.host, self.port = host, port
        self.path_prefix = path_prefix
        self.extra_headers = extra_headers or dict()
        self.oauth2_user = oauth2_user
        self.scopes = scopes
        self.client_id = client_id
        self.client_grant_type = client_grant_type
        self.application_id = application_id
        self.application_class = application_class
        self.organisation_id = organisation_id
        self.max_request_size = max_request_size

    async def serve(self):
        # Configure the aiohttp web server application for the proxy.
        proxy_app = aiohttp.web.Application(client_max_size=self.max_request_size)
        proxy_app.router.add_route("*", "/{path:.*}", self._handle_request)
        proxy_runner = aiohttp.web.AppRunner(proxy_app)
        await proxy_runner.setup()

        # Start the server.
        LOG.info("Starting proxy server.", host=self.host, port=self.port)
        proxy_site = aiohttp.web.TCPSite(proxy_runner, self.host, self.port)
        return await proxy_site.start()

    async def _handle_request(self, request):
        relative_url = str(request.rel_url)
        normalised_path_prefix = self.path_prefix.rstrip("/")
        if not relative_url.startswith(f"{normalised_path_prefix}/"):
            return aiohttp.web.Response(
                status=404,
                body="Not found",
            )
        destination_relative_url = relative_url.removeprefix(self.path_prefix)
        new_url = urllib.parse.urljoin(
            self.destination_url, str(destination_relative_url).lstrip("/")
        )
        LOG.info("Proxying request.", source=str(request.url), destination=new_url)

        new_headers = request.headers.copy()
        new_headers.extend(self.extra_headers)
        new_headers["X-Forwarded-Prefix"] = normalised_path_prefix
        new_headers["X-Forwarded-Host"] = request.headers["host"]
        new_headers["X-Forwarded-Port"] = str(request.url.port)
        new_headers["X-Api-OAuth2-User"] = self.oauth2_user
        new_headers["X-Api-OAuth2-Scope"] = self.scopes
        new_headers["X-Api-OAuth2-Client-Id"] = self.client_id
        new_headers["X-Api-OAuth2-Grant-Type"] = self.client_grant_type
        new_headers["Authorization"] = f"Bearer {self.jwt_factory.encode_jwt()}"
        if self.application_class != "public":
            new_headers["X-Api-Developer-App-Id"] = self.application_id
            new_headers["X-Api-Developer-App-Class"] = self.application_class
        new_headers["X-Api-Org-Name"] = self.organisation_id

        forwarded_request = aiohttp.client.request(
            request.method,
            new_url,
            headers=new_headers,
            data=await request.read() if request.body_exists else None,
            allow_redirects=False,
        )

        try:
            async with forwarded_request as response:
                # We need to remove the transfer encoding headers since we read the body and
                # retransmit it. The aiohttp library handles chunked encodings for us and so we
                # don't want to claim to the upstream client that the body is chunked when it is
                # not.
                response_headers = response.headers.copy()
                response_headers.pop("transfer-encoding", None)

                # Prepare the returned response from the received response.
                returned_response = aiohttp.web.StreamResponse(
                    headers=response_headers,
                    status=response.status,
                    reason=response.reason,
                )
                await returned_response.prepare(request)

                # Read and forward the body by iterating over the stream.
                async for data in response.content.iter_any():
                    await returned_response.write(data)
                await returned_response.write_eof()
        except aiohttp.client.ClientConnectionError as e:
            LOG.error("Failed to connect to destination", error=str(e))
            raise aiohttp.web.HTTPBadGateway()

        return returned_response
