import datetime
from typing import Any, Optional
from urllib.parse import urlsplit

from cryptography import x509
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.x509.oid import NameOID
from jwcrypto import jwk, jwt


class JwtFactory:
    def __init__(
        self,
        *,
        issuer: str,
        audience: str,
        authorised_party: str,
        key_id: str,
        lifespan=datetime.timedelta(minutes=5)
    ):
        self._issuer = issuer
        self._audience = audience
        self._authorised_party = authorised_party
        self._key_id = key_id
        self._lifespan = lifespan

        # Generate the RSA key used to sign tokens.
        self._private_key = rsa.generate_private_key(
            public_exponent=65537,
            key_size=2048,
        )

        # Generate a private key.
        self._private_key_jwk = jwk.JWK.generate(
            kty="RSA", size=2048, public_exponent=65537, use="sig", kid=self._key_id
        )

        # Generate a certificate corresponding to that key.
        name = x509.Name(
            [
                x509.NameAttribute(NameOID.COMMON_NAME, urlsplit(issuer).hostname),
            ]
        )
        private_key = self._private_key_jwk.get_op_key("sign")
        cert_builder = (
            x509.CertificateBuilder()
            .subject_name(name)
            .issuer_name(name)
            .public_key(private_key.public_key())
            .serial_number(x509.random_serial_number())
            .not_valid_before(
                datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(days=1)
            )
            .not_valid_after(
                datetime.datetime.now(datetime.timezone.utc) + datetime.timedelta(days=10)
            )
            .sign(private_key, hashes.SHA256())
        )
        self._cert = cert_builder.public_bytes(serialization.Encoding.PEM)

    @property
    def certs_document(self) -> dict[str, str]:
        """A dictionary mapping key id to certificate."""
        return {self._key_id: self._cert.decode("ascii")}

    @property
    def jwks_document(self) -> dict[str, Any]:
        """A dictionary containing the JWK keys as a JWKS document."""
        return {"keys": [self._private_key_jwk.export_public(as_dict=True)]}

    def encode_jwt(self, payload: Optional[dict[str, Any]] = None) -> str:
        """
        Encode a JWT. The iss, aud, azp, iat, exp and nbf claims are set. Additional claims can
        be set by passing a payload dictionary.
        """
        iat = datetime.datetime.now(datetime.timezone.utc)
        nbf = iat - datetime.timedelta(minutes=1)
        exp = iat + self._lifespan
        token = jwt.JWT(
            header={"kid": self._key_id, "alg": "RS256"},
            claims={
                "iat": int(iat.timestamp()),
                "nbf": int(nbf.timestamp()),
                "exp": int(exp.timestamp()),
                "aud": self._audience,
                "iss": self._issuer,
                "azp": self._authorised_party,
                **(payload if payload is not None else {}),
            },
            algs=["RS256"],
        )
        token.make_signed_token(self._private_key_jwk)
        return token.serialize()
