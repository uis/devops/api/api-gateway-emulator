import asyncio
import contextlib
import logging
from secrets import token_hex
from typing import Any, Optional, Union
from urllib.parse import urljoin

import pydantic
import structlog
import uvicorn
from caseconverter import camelcase
from fastapi import FastAPI, Request
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles

from .jwt import JwtFactory
from .logconfig import configure_logging, logging_dict_config
from .reverseproxy import ReverseProxy

LOG = structlog.get_logger()


class Settings(pydantic.BaseSettings):
    verbose_logging: bool = True

    proxy_host: str = "0.0.0.0"
    proxy_port: int = 9000

    console_host: str = "0.0.0.0"
    console_port: int = 9001
    console_ui_dir: Optional[pydantic.DirectoryPath]

    proxy_destination_url: pydantic.AnyHttpUrl = "http://localhost:8000"
    proxy_path_prefix: str = "/"

    oauth2_user: pydantic.EmailStr = "test-application-id@application.api.apps.cam.ac.uk"
    scopes: str = ""
    client_id: str = "test-client-id"
    client_grant_type: str = "client_credentials"
    application_id: str = "test-application-id"
    application_class: str = "public"
    organisation_id: str = "test-organisation-id"

    jwt_iss: str = "https://localhost:9001/"
    jwt_aud: str = "https://audience.invalid/"
    jwt_azp: str = "api-gateway@api-meta-2555105a.iam.gserviceaccount.com"

    jwt_key_id: str = pydantic.Field(default_factory=token_hex)

    # Max size (in bytes) of an HTTP request that can be proxied
    max_request_size = 100 * (2**20)

    class Config:
        env_prefix = "gateway_"


settings = Settings()

jwt_factory = JwtFactory(
    issuer=settings.jwt_iss,
    audience=settings.jwt_aud,
    authorised_party=settings.jwt_azp,
    key_id=settings.jwt_key_id,
)

reverse_proxy = ReverseProxy(
    destination_url=settings.proxy_destination_url,
    jwt_factory=jwt_factory,
    host=settings.proxy_host,
    port=settings.proxy_port,
    path_prefix=settings.proxy_path_prefix,
    oauth2_user=settings.oauth2_user,
    scopes=settings.scopes,
    client_id=settings.client_id,
    client_grant_type=settings.client_grant_type,
    application_id=settings.application_id,
    application_class=settings.application_class,
    organisation_id=settings.organisation_id,
    max_request_size=settings.max_request_size,
)


@contextlib.asynccontextmanager
async def api_lifespan(app):
    "Manage background tasks which run for the lifespan of the API."
    reverse_proxy_task = asyncio.create_task(reverse_proxy.serve())
    yield
    reverse_proxy_task.cancel()
    with contextlib.suppress(asyncio.CancelledError):
        LOG.info("Stopping reverse proxy.")
        await reverse_proxy_task


app = FastAPI(lifespan=api_lifespan)
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_headers=["*"],
    allow_methods=["*"],
    allow_credentials=True,
)


class ProxyStatus(pydantic.BaseModel):
    destination_url: pydantic.AnyHttpUrl
    path_prefix: str
    extra_headers: dict[str, Union[list[str], str]]
    host: str
    port: int
    oauth2_user: str
    scopes: str
    client_id: str
    client_grant_type: str
    application_id: str
    application_class: str
    organisation_id: str
    max_request_size: int

    class Config:
        alias_generator = camelcase
        allow_population_by_field_name = True
        orm_mode = True


class ProxyConfigurationPatch(pydantic.BaseModel):
    path_prefix: Optional[str]
    oauth2_user: Optional[pydantic.EmailStr]
    scopes: Optional[str]
    client_id: Optional[str]
    client_grant_type: Optional[str]
    application_id: Optional[str]
    application_class: Optional[str]
    organisation_id: Optional[str]

    @pydantic.validator("path_prefix")
    def path_prefix_must_have_leading_slash(cls, v):
        if not v.startswith("/"):
            raise ValueError("Path prefix must have a leading slash.")
        return v

    class Config:
        alias_generator = camelcase
        allow_population_by_field_name = True


@app.get("/certs")
def get_certs() -> dict[str, str]:
    return jwt_factory.certs_document


@app.get("/.well-known/jwks.json")
def get_jwks() -> dict[str, Any]:
    return jwt_factory.jwks_document


@app.get("/.well-known/openid-configuration")
def get_openid_configuration(request: Request) -> dict[str, str]:
    return {
        "issuer": settings.jwt_iss,
        "jwks_uri": urljoin(str(request.base_url), ".well-known/jwks.json"),
    }


@app.get("/proxy/status")
def get_status() -> ProxyStatus:
    return reverse_proxy


@app.patch("/proxy/configuration")
def put_configuration(configuration: ProxyConfigurationPatch) -> ProxyStatus:
    fields = [
        "path_prefix",
        "oauth2_user",
        "scopes",
        "client_id",
        "client_grant_type",
        "application_id",
        "application_class",
        "organisation_id",
    ]

    for field in fields:
        if getattr(configuration, field) is not None:
            setattr(reverse_proxy, field, getattr(configuration, field))

    return reverse_proxy


# Mount the console UI. Default to the statics directory within this package unless a directory is
# provided from outside.
app.mount(
    "/",
    StaticFiles(directory=settings.console_ui_dir, packages=["api_gateway_emulator"], html=True),
    name="ui",
)


def make_console_server(*, host, port):
    config = uvicorn.Config(
        app,
        host=settings.console_host,
        port=settings.console_port,
        log_config=logging_dict_config(
            "uvicorn", level=logging.INFO if settings.verbose_logging else logging.WARN
        ),
    )
    return uvicorn.Server(config)


def main():
    configure_logging(verbose=settings.verbose_logging)
    api_server = make_console_server(host=settings.console_host, port=settings.console_port)
    loop = asyncio.get_event_loop()
    loop.run_until_complete(api_server.serve())
